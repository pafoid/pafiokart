package
{
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.ui.IApplication;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	
	[SWF(frameRate="24")]
	public class PafioKart extends Sprite implements IApplication
	{
		private var _appFacade:AppFacade;
		
		public function PafioKart()
		{
			super();
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.setOrientation(StageOrientation.ROTATED_RIGHT);
			
			_appFacade = new AppFacade(this);
		}
	}
}