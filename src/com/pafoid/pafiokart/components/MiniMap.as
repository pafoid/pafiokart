package com.pafoid.pafiokart.components
{
	import com.pafoid.display.Color;
	import com.pafoid.geometry.polygon.SquareTriangle;
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.game.controllers.GameController;
	import com.pafoid.pafiokart.game.events.GameEvent;
	import com.pafoid.pafiokart.game.model.objectTypes.SegmentType;
	import com.pafoid.pafiokart.game.model.objects.Circuit;
	import com.pafoid.pafiokart.game.model.objects.Segment;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.ColorTransform;

	public class MiniMap extends MovieClip
	{
		public static const NORTH:int 		= 0;
		public static const SOUTH:int 		= 1;
		public static const EAST:int 		= 2;
		public static const WEST:int 		= 3;
		
		private var _circuit:Circuit;
		private var _map:Array = [];
		private var _appFacade:AppFacade;
		private var _circuitController:GameController;
		private var _squareWidth:int = 10;
		
		public function MiniMap(appFacade:AppFacade, circuit:Circuit, circuitController:GameController) 
		{
			_appFacade = appFacade;
			_circuit = circuit;
			_circuitController = circuitController;
			
			generateMap();
		}
		
		private function generateMap():void
		{
			var posX:int = 0;
			var posY:int = 0;
			var direction:int = EAST;
			var prevSegmentType:int;
			
			//Square
			var squares:Array = [];
			for each (var segment:Segment in _circuit.circuitArray) 
			{
				for (var i:int = 0; i < segment.segmentLength; i++) 
				{
					var square:Sprite = getSquare(_squareWidth); 
					
					switch(segment.segmentType){
						case SegmentType.STRAIGHT:
						case SegmentType.UPHILL:
						case SegmentType.DOWNHILL:
								switch(direction){
									case NORTH:
										posY -= _squareWidth;
										break;
									case SOUTH:
										posY += _squareWidth;
										break;
									case EAST:
										posX += _squareWidth;
										break;
									case WEST:
										posX -= _squareWidth;
										break;
								}
							break;
						case SegmentType.CURVED_RIGHT:
						case SegmentType.UPHILL_RIGHT:
						case SegmentType.DOWNHILL_RIGHT:
							var corner:Sprite = getSquare(_squareWidth); 
							switch(direction){
								case NORTH:
									posY -= _squareWidth;
									corner.x = posX;
									corner.y = posY;					
									addChild(corner);
									_map.push(corner);
									
									posX += _squareWidth;
									direction = EAST;
									break;
								case SOUTH:
									posY += _squareWidth;
									corner.x = posX;
									corner.y = posY;					
									addChild(corner);
									_map.push(corner);
									
									posX -= _squareWidth;
									direction = WEST;
									break;
								case EAST:
									posX += _squareWidth;

									corner.x = posX;
									corner.y = posY;					
									addChild(corner);
									_map.push(corner);
									
									posY += _squareWidth;
									direction = SOUTH;
									break;
								case WEST:
									posX -= _squareWidth;
									
									corner.x = posX;
									corner.y = posY;					
									addChild(corner);
									_map.push(corner);
									
									posY -= _squareWidth;
									direction = NORTH;
									break;
							}
							break;
					}
					
					square.x = posX;
					square.y = posY;					
					addChild(square);
					_map.push(square);
				}
			}
		}
		
		public function updateMap(event:GameEvent):void{
			/*trace("updateMap():: _circuitController.currentProgress :", _circuitController.currentProgress);
			
			var square:Sprite = _map[_circuitController.currentProgress-1];
			resetSquare(square);
			
			square = _map[_circuitController.currentProgress];
			setActiveSquare(square);*/			
		}
		
		private function setActiveSquare(square:Sprite):void{
			if(square){
				square.graphics.clear();
				square.graphics.lineStyle(0,0);
				square.graphics.beginFill(Color.RED);
				square.graphics.drawRect(0,0,_squareWidth,_squareWidth);
				square.graphics.endFill();
			}
		}
		
		private function resetSquare(square:Sprite):void{
			if(square){
				square.graphics.lineStyle(0,0);
				square.graphics.beginFill(Color.WHITE);
				square.graphics.drawRect(0,0,_squareWidth,_squareWidth);
				square.graphics.endFill();
			}
		}
		
		private function getSquare(width:int):Sprite{ 
			var square:Sprite = new Sprite();
			square.graphics.lineStyle(0,0);
			square.graphics.beginFill(Color.WHITE);
			square.graphics.drawRect(0,0,_squareWidth,_squareWidth);
			square.graphics.endFill();
			
			return square;
		}
		
		private function getCorner(width:int):Sprite{ 
			var corner:Sprite = SquareTriangle.newTriangleSprite(width);			
			return corner;
		}			
	}
}