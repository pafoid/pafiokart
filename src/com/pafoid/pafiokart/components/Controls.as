package com.pafoid.pafiokart.components
{
	import com.pafoid.display.Color;
	import com.pafoid.geometry.CircleCreator;
	import com.pafoid.math.MathUtils;
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.screen.Screen;
	import com.pafoid.ui.controls.dpad.DpadControl;
	
	import flash.display.Sprite;
	import flash.events.AccelerometerEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.sensors.Accelerometer;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;

	public class Controls
	{
		private var _appFacade:AppFacade;
		private var _rootView:Screen;
		
		private var _dpad:DpadControl;
		private var _btnA:Sprite;//Gas
		private var _btnB:Sprite;//Jump
		private var _btnC:Sprite;//Break
		
		private var _isGasDown:Boolean;
		private var _isBreakDown:Boolean;
		private var _isJumpDown:Boolean;
		
		private var _accelerometer:Accelerometer;
		private var _steeringValue:Number = 0;
		
		public function Controls(appFacade:AppFacade, rootView:Screen)
		{
			_appFacade = appFacade;	
			_rootView = rootView;
		}
		
		public function addToStage():void{
			//_dpad = new DpadControl(300, 300, this);
			var posX:Number = _appFacade.stage.fullScreenWidth * 0.8;
			var margin:Number = 25;
			var posY:Number = _appFacade.stage.fullScreenHeight * 0.9;
				
			
			_btnA = generateGameButton("A", posX, posY, onBtnGasDown, onBtnGasUp);
			_btnB = generateGameButton("B", posX + _btnA.width + margin, posY, onBtnJumpDown, onBtnJumpUp);
			_btnC = generateGameButton("C", _btnB.x + _btnB.width + margin, posY, onBtnBreakDown, onGameBreakUp);
			
			if (Accelerometer.isSupported){ //Device
				_accelerometer = new Accelerometer(); 
				_accelerometer.addEventListener(AccelerometerEvent.UPDATE, accelerometerHandler); 
			}else{ //Desktop
				_appFacade.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, 0, true);
				_appFacade.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp, false, 0, true);
			} 
		}
		
		private function generateGameButton(text:String, posX:Number, posY:Number, downFunction:Function, upFunction:Function):Sprite{
			var button:Sprite = CircleCreator.createCircle(posX, posY, 25, 0, 0, Color.WHITE, 0.8, 0, _rootView);
			var label:TextField = new TextField();
			var tf:TextFormat = new TextFormat("pafoid EightBitWonder", 25, Color.BLACK, true);
			label.autoSize = "center";
			label.defaultTextFormat = tf;
			label.embedFonts = true;
			label.text = text;
			label.x = -label.width/2;
			label.y = -label.height/2;
			label.selectable = false;
			button.addChild(label);
			button.x = posX;
			button.y = posY;
			
			button.addEventListener(MouseEvent.MOUSE_DOWN, downFunction, false, 0, true);
			button.addEventListener(MouseEvent.MOUSE_UP, upFunction, false, 0, true);
			button.addEventListener(MouseEvent.MOUSE_OUT, upFunction, false, 0, true);
			
			return button;
		}
		
		//Handlers
		private function onBtnGasUp(event:MouseEvent):void{
			_isGasDown = false;
		}
		
		private function onBtnJumpUp(event:MouseEvent):void{
			_isJumpDown = false;
		}
		
		private function onGameBreakUp(event:MouseEvent):void{
			_isBreakDown = false;
		}
		
		private function onBtnGasDown(event:MouseEvent):void{
			_isGasDown = true;
		}
		
		private function onBtnJumpDown(event:MouseEvent):void{
			_isJumpDown = true;	
		}
		
		private function onBtnBreakDown(event:MouseEvent):void{
			_isBreakDown = true;
		}
		
		private function accelerometerHandler(event:AccelerometerEvent):void
		{
			_steeringValue = MathUtils.round(event.accelerationX, 2);
		}
		
		private function onKeyDown(event:KeyboardEvent):void{
			var key:uint = event.keyCode;
			
			switch(key){
				case Keyboard.LEFT:
				case Keyboard.S:
				case Keyboard.NUMPAD_4:
					_steeringValue += 0.05;
					break;
				case Keyboard.RIGHT:
				case Keyboard.D:
				case Keyboard.NUMPAD_6:
					_steeringValue -= 0.05;
					break;
				case Keyboard.SPACE:
					_isJumpDown = true;
					break;
				case Keyboard.DOWN:
					_isBreakDown = true;
					break;
				case Keyboard.UP:
					_isGasDown = true;
					break;
			}
		}
		
		private function onKeyUp(event:KeyboardEvent):void{
			var key:uint = event.keyCode;
			
			switch(key){
				case Keyboard.SPACE:
					_isJumpDown = false;
					break;
				case Keyboard.DOWN:
					_isBreakDown = false;
					break;
				case Keyboard.UP:
					_isGasDown = false;
					break;
			}
		}
		
		public function tick():void{
		
		}

		public function destroy():void{
			_rootView = null;
			_appFacade = null;
		}
		
		public function get isGasDown():Boolean
		{
			return _isGasDown;
		}

		public function get isBreakDown():Boolean
		{
			return _isBreakDown;
		}

		public function get isJumpDown():Boolean
		{
			return _isJumpDown;
		}

		public function get steeringValue():Number
		{
			return _steeringValue;
		}

	}
}