package com.pafoid.pafiokart.components
{
	import com.pafoid.pafiokart.AppFacade;
	
	import flash.display.Sprite;
	
	public class HUD
	{
		private var _appFacade:AppFacade;
		
		public function HUD(appFacade)
		{
			_appFacade = appFacade;
		}
		
		public function destroy():void{
			_appFacade = null;
		}
	}
}