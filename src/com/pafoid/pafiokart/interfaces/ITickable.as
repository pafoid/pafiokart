package com.pafoid.pafiokart.interfaces
{
	public interface ITickable
	{
		function tick():void;
	}
}