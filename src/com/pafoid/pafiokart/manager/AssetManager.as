package com.pafoid.pafiokart.manager
{
	import com.pafoid.pafiokart.AppFacade;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;

	public class AssetManager extends EventDispatcher
	{
		private var _appFacade:AppFacade;
		
		private var _assetsArray:Array;
		private var _assetsDic:Dictionary = new Dictionary();
		private var _jsonURL:String = "json/Assets.json";
		private var _loader:URLLoader;
		
		private var _callBack:Function;
		
		private var _assetCount:int = 0;
		private var _loadedAssets:int = 0;
		
		public function AssetManager(appFacade:AppFacade)
		{
			_appFacade = appFacade;
			loadAssetsJSON(_jsonURL);
		}
		
		private function loadAssetsJSON(url:String):void
		{
			_loader = new URLLoader;
			_loader.addEventListener(Event.COMPLETE, onAssetsJSONLoaded, false, 0, true);
			_loader.load(new URLRequest(url));
			
		}
		
		protected function onAssetsJSONLoaded(event:Event):void
		{
			_loader.removeEventListener(Event.COMPLETE, onAssetsJSONLoaded);
			_assetsArray = JSON.parse(_loader.data).assets;
			_loader = null;
			
			loadAssets("default", onDefaultAssetsLoaded);
		}
		
		public function loadAssets(screenName:String, callBack:Function):void{
			//_assetsDic =  new Dictionary();
			_callBack = callBack;
			_loadedAssets = 0;
			
			for each (var screen:Object in _assetsArray) 
			{
				if(screen.id == screenName){
					_assetCount = 0;
					
					for each (var asset:Object in screen.assets) 
					{
						var loader:Loader = new Loader();
						loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onAssetLoaded, false, 0, true);
						loader.load(new URLRequest(asset.url));
						
						_assetCount ++;
					}					
					
					if(_assetCount == 0)
						callBack();
				}
			}
		}
		
		protected function onAssetLoaded(event:Event):void
		{
			var loader:Loader = LoaderInfo(event.target).loader;
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onAssetLoaded);
			
			var assetSWF:MovieClip = loader.content as MovieClip;
			for (var i:int = 0; i < assetSWF.numChildren; i++) 
			{
				var asset:* = assetSWF.getChildAt(i);
				_assetsDic[asset.name] = asset;
			}
			
			_loadedAssets ++;
			
			if(_loadedAssets == _assetCount)
				_callBack();
			
		}
		
		private function onDefaultAssetsLoaded():void{
			dispatchEvent(new Event(Event.COMPLETE, true, false));
		}
		
		public function getAssetById(id:String):MovieClip{
			var mc:MovieClip = _assetsDic[id];
			var targetClass:Class = Object(mc).constructor;
			var result:MovieClip = new targetClass() as MovieClip;
			
			if(result){
				result.gotoAndStop(1);
			}else{
				throw new Error("Asset with id : "+id+" could not be found");
			}
			
			return result;
		}

		public function set jsonURL(value:String):void
		{
			_jsonURL = value;
		}
	}
}