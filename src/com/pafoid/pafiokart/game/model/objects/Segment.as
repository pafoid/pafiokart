package com.pafoid.pafiokart.game.model.objects
{
	public class Segment
	{
		private var _segmentType:int;
		private var _segmentLength:int;
		
		public function Segment(segmentType:int = 0, segmentLength:int = 150)
		{
			_segmentType = segmentType;
			_segmentLength = segmentLength;
		}

		public function get segmentLength():int
		{
			return _segmentLength;
		}

		public function set segmentLength(value:int):void
		{
			_segmentLength = value;
		}

		public function get segmentType():int
		{
			return _segmentType;
		}

		public function set segmentType(value:int):void
		{
			_segmentType = value;
		}

	}
}