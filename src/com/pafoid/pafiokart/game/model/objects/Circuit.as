package com.pafoid.pafiokart.game.model.objects
{
	public class Circuit
	{
		private var _id:String;
		private var _circuitArray:Array;
		private var _segmentArray:Array;
		private var _circuitType:int;
		private var _laps:int;
		
		public function Circuit(id:String, circuitArray:Array, circuitType:int = 0, laps:int = 3)
		{
			_id = id;
			_circuitArray = circuitArray;
			_segmentArray = generateArray(circuitArray);
			_circuitType = circuitType;
			_laps = laps;
		}
		
		public function get segmentArray():Array
		{
			return _segmentArray;
		}

		private function generateArray(circuitArray:Array):Array
		{
			var array:Array = [];
			
			for each (var segment:Segment in circuitArray) 
			{
				for (var i:int = 0; i < segment.segmentLength; i++) 
				{
					var seg:Segment = new Segment(segment.segmentType, 1);
					array.push(seg);
				}				
			}			
			
			return array;
		}
		
		public function get laps():int
		{
			return _laps;
		}

		public function set laps(value:int):void
		{
			_laps = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

		public function get circuitType():int
		{
			return _circuitType;
		}

		public function set circuitType(value:int):void
		{
			_circuitType = value;
		}

		public function get circuitArray():Array
		{
			return _circuitArray;
		}
		
		public function get length():int{
			var length:int = 0;
			
			for each (var segment:Segment in _circuitArray) 
			{
				length += segment.segmentLength;
			}
			
			return length;
		}
	}
}