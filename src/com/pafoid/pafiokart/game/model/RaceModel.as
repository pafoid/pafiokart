package com.pafoid.pafiokart.game.model
{
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.game.model.objects.Circuit;
	
	import flash.utils.Dictionary;

	public class RaceModel
	{
		public var circuitArray:Array;
		public var circuitDic:Dictionary;
		
		public function RaceModel(_circuitArray:Array = null, _circuitDic:Dictionary = null)
		{
			circuitArray = _circuitArray;
			circuitDic = _circuitDic; 
		}
		
		public function getCircuitByName(id:String):Circuit{
			return circuitDic[id];
		}
		
		public function getCircuitById(id:int):Circuit{
			return circuitArray[id];
		}
		
		public function destroy():void{
			circuitArray = null;
			circuitDic = null;
		}
	}
}