package com.pafoid.pafiokart.game.model
{
	public class PlayerModel
	{
		public static const DEFAULT_WEIGHT:Number = 147; // kg
		
		public var name:String;
		public var kart:KartModel;
		private var _playerWeight:Number = 80; // 68 kg
		
		
		public function PlayerModel(_name:String = "Player 1", _kart:KartModel = null)
		{
			kart = kart ? kart : new KartModel();
		}
		
		public function get weight():Number{
			return _playerWeight + kart.weight;
		}
	}
}