package com.pafoid.pafiokart.game.model
{
	public class KartModel
	{
		public static const MAX_ACCELERATION:Number 		= 10; // km/h
		public static const MAX_TOP_SPEED:Number 			= 50; // km/h
		public static const MAX_WEIGHT:Number 				= 70; // kg
		public static const MAX_TOUGHNESS:Number 			= 10;
		public static const MAX_HANDLING:Number 			= 10;
		public static const MAX_BREAKING:Number 			= 10; // km/h
		public static const MAX_REVERSE_TOP_SPEED:Number 	= 30; // km/h
		
		public static const DEFAULT_ACCELERATION:Number 		= 0.5;
		public static const DEFAULT_TOP_SPEED:Number 			= 32.5;
		public static const DEFAULT_WEIGHT:Number 				= 79;
		public static const DEFAULT_TOUGHNESS:Number 			= 0;
		public static const DEFAULT_HANDLING:Number 			= 5;
		public static const DEFAULT_BREAKING:Number 			= 0.5;
		public static const DEFAULT_REVERSE_TOP_SPEED:Number 	= 1.5;
		
		public var acceleration:Number 		= DEFAULT_ACCELERATION;
		public var topSpeed:Number			= DEFAULT_TOP_SPEED;
		public var weight:Number 			= DEFAULT_WEIGHT;
		public var toughness:Number 		= DEFAULT_TOUGHNESS;
		public var handling:Number			= DEFAULT_TOUGHNESS;
		public var braking:Number 			= DEFAULT_BREAKING;
		public var reverseTopSpeed:Number	= DEFAULT_REVERSE_TOP_SPEED;
		
		public function KartModel()
		{
		}
	}
}
