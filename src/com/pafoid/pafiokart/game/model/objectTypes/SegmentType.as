package com.pafoid.pafiokart.game.model.objectTypes
{
	public class SegmentType
	{
		public static const STRAIGHT:int 		= 0;
		public static const CURVED_LEFT:int		= 1;
		public static const CURVED_RIGHT:int	= 2;
		public static const UPHILL:int			= 3;
		public static const DOWNHILL:int		= 4;
		public static const UPHILL_LEFT:int		= 5;
		public static const UPHILL_RIGHT:int	= 6;
		public static const DOWNHILL_LEFT:int	= 7;
		public static const DOWNHILL_RIGHT:int	= 8;
		
		public static function toString(segmentType:int):String{
			var result:String;
			switch(segmentType){
				case STRAIGHT:
					result = "STRAIGHT";
					break;
				case CURVED_LEFT:
					result = "CURVED_LEFT";
					break;
				case CURVED_RIGHT:
					result = "CURVED_RIGHT";
					break;
				case UPHILL:
					result = "UPHILL";
					break;
				case DOWNHILL:
					result = "DOWNHILL";
					break;
				case UPHILL_LEFT:
					result = "UPHILL_LEFT";
					break;
				case UPHILL_RIGHT:
					result = "UPHILL_RIGHT";
					break;
				case DOWNHILL_LEFT:
					result = "DOWNHILL_LEFT";
					break;
				case DOWNHILL_RIGHT:
					result = "DOWNHILL_RIGHT";
					break;				
			}
			
			return result;
		}
	}
}