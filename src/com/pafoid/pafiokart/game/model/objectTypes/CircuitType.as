package com.pafoid.pafiokart.game.model.objectTypes
{
	public class CircuitType
	{
		public static const TARMAC:int		= 0;
		public static const DIRT:int		= 1;
		
		public static const BONUS:int		= 2;
	}
}