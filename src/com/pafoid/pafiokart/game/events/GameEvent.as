package com.pafoid.pafiokart.game.events
{
	import flash.events.Event;
	
	public class GameEvent extends Event
	{
		public static const UPDATE_PROGRESS:String 				= "updateProgress";
		public static const START_RACE:String 					= "startRace";
		public static const PRE_RACE_MSG_COMPLETE:String 		= "preRaceMsgComplete";
		
		public function GameEvent(type:String)
		{
			super(type, true, false);
		}
	}
}