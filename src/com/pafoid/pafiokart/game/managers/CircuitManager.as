package com.pafoid.pafiokart.game.managers
{
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.game.model.RaceModel;
	import com.pafoid.pafiokart.game.model.objects.Circuit;
	import com.pafoid.pafiokart.game.model.objects.Segment;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;

	public class CircuitManager extends EventDispatcher
	{
		private var _appFacade:AppFacade;
		private var _raceModel:RaceModel;
		
		private var _loader:URLLoader;
		
		private var _callBack:Function;
		
		private var _assetCount:int = 0;
		private var _loadedAssets:int = 0;
		
		public function CircuitManager(appFacade:AppFacade, raceModel:RaceModel)
		{
			_appFacade = appFacade;
			_raceModel = raceModel;
			
			loadGameJSON();
		}
		
		private function loadGameJSON():void
		{
			_loader = new URLLoader;
			_loader.addEventListener(Event.COMPLETE, onAssetsJSONLoaded, false, 0, true);
			_loader.load(new URLRequest("json/Game.json"));
			
		}
		
		protected function onAssetsJSONLoaded(event:Event):void
		{
			_loader.removeEventListener(Event.COMPLETE, onAssetsJSONLoaded);
			var circuitObjectsArray:Array = JSON.parse(_loader.data).game.circuits;
			_loader = null;
			
			_raceModel.circuitArray = [];
			_raceModel.circuitDic = new Dictionary();
			
			for each (var circuitObject:Object in circuitObjectsArray) 
			{
				var segmentsArray:Array = circuitObject.segments;
				var segments:Array = [];
				for each (var segmentObject:Object in segmentsArray) 
				{
					var segment:Segment = new Segment(segmentObject.segmentType, segmentObject.segmentLength);
					segments.push(segment);
				}
				
				var circuit:Circuit = new Circuit(circuitObject.id, segments, circuitObject.segmentType);
				_raceModel.circuitArray.push(circuit);
				_raceModel.circuitDic[circuit.id] = circuit;
			}
			
			
			dispatchEvent(new Event(Event.COMPLETE, true, false));
		}
		
		
		public function destroy():void{
		
		}
	}
}