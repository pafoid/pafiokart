package com.pafoid.pafiokart.game.controllers
{
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.components.Controls;
	import com.pafoid.pafiokart.game.events.GameEvent;
	import com.pafoid.pafiokart.game.model.PlayerModel;
	import com.pafoid.pafiokart.game.model.objectTypes.SegmentType;
	import com.pafoid.pafiokart.game.model.objects.Circuit;
	import com.pafoid.pafiokart.game.model.objects.Segment;
	import com.pafoid.pafiokart.interfaces.ITickable;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.EventDispatcher;

	public class GameController extends EventDispatcher implements ITickable
	{
		private var _appFacade:AppFacade;
		
		private var _circuitView:Sprite;

		//Depth of the visible road
		private const roadLines:int = 150;
		
		//Dimensions of the play area.
		private const resX:int = 480;
		private const resY:int = 320;
		
		//Line of the player's car.
		private const noScaleLine:int = 8;
		
		private var zMap:Array = [];
		private var lines:Array = [];
		private var halfWidth:Number;
		private var lineDepth:int;
		private const widthStep:Number = 1;
		private var playerZ:Number;
		private var _speed:Number = 0;
		private var _textureOffset:int = 100;
		
		private var _linePosX:Number;//Each line's x position
		private var _segmentCurveAmount:Number;//Curve amount per segment
		private var _curveSharpness:Number = 0.02;//Curve amount per line
		
		private var _currentSegmentY:int = roadLines;
		private var _currentSegmentX:int = 1;
		private var _currentSegment:int = SegmentType.STRAIGHT;
		private var _nextSegment:int = SegmentType.STRAIGHT;
		
		private var _linePosY:Number;//Each line's y position
		private var _segmentSteepnessAmount:Number;//Steepness amount per segment
		private var _steepness:Number = 0.01;//Steepness amount per line, A little less steep than the curves.
		
		private var _circuit:Circuit;
		private var _segmentProgress:int = 0;
		private var _currentLap:int = 1;
		
		private var _roadTypes:Array = [SegmentType.CURVED_RIGHT, SegmentType.STRAIGHT, SegmentType.CURVED_RIGHT, SegmentType.STRAIGHT, SegmentType.CURVED_RIGHT];
		private var _playerModel:PlayerModel;
		private var _controls:Controls;
		private var _isFirstTick:Boolean = true;
		private var _startBanner:MovieClip;
		
		public function GameController(appFacade:AppFacade, circuit:Circuit, playerModel:PlayerModel, controls:Controls) 
		{
			_appFacade = appFacade;
			_circuit = circuit;
			_playerModel = playerModel;
			_controls = controls;
			
			initCircuit();
		}

		public function get speed():Number
		{
			return _speed;
		}

		public function set speed(value:Number):void
		{
			_speed = value;
		}

		private function initCircuit():void{
			_circuitView ||= new Sprite();
			
			//Set zMap
			for (var i:int = 0; i < roadLines; i++)
			{
				zMap.push(1 / (i - resY / 2));
			}
			
			playerZ = 100 / zMap[noScaleLine];
			for (i = 0; i < roadLines; i++)
			{
				zMap[i] *=  playerZ;
			}
			
			//Add road lines to screen
			lineDepth = _circuitView.numChildren;
			
			for (i = 0; i < roadLines; i++)
			{
				var line:MovieClip = _appFacade.assetManager.getAssetById("road");
				lines.push(line);
				_circuitView.addChildAt(line, lineDepth);
				line.x = resX / 2;
				line.y = resY - i;
			}
			
			//Adjust scale for perspective
			halfWidth = resX / 2;
			for (i = 0; i < roadLines; i++)
			{
				lines[i].scaleX = halfWidth / 60 - 1.2;
				halfWidth -=  widthStep;
			}
			
			//Set to the beggining
			_currentSegment = Segment(_circuit.segmentArray[_segmentProgress]).segmentType;
			_nextSegment = Segment(_circuit.segmentArray[_segmentProgress + 1]).segmentType;
				
			trace("\ncurrentSegment : " + SegmentType.toString(_currentSegment) + " nextSegment : "+SegmentType.toString(_nextSegment));
			
			_currentSegmentY = roadLines;
			
			//Add start banner
			var startingLine:int = 115;
			_startBanner = _appFacade.assetManager.getAssetById("startBanner");
			_startBanner.scaleY = lines[startingLine-5].scaleY;
			_startBanner.x = lines[startingLine].x;
			_startBanner.y = lines[startingLine].y - 95;
			_circuitView.addChild(_startBanner);
			
			tick();
		}
		
		public function tick():void
		{
			//Calculate speed
			if(_controls.isBreakDown){
				if(Math.abs(speed) <= _playerModel.kart.reverseTopSpeed && speed > 0)
					speed -= _playerModel.kart.braking + ((_playerModel.weight - PlayerModel.DEFAULT_WEIGHT)/10);
			}else if(_controls.isGasDown){
				if(speed <= _playerModel.kart.topSpeed)
					speed += (_playerModel.kart.acceleration^2) - ((_playerModel.weight - PlayerModel.DEFAULT_WEIGHT)/10);
			}else{
				if(speed > 0)
					speed -= _playerModel.kart.acceleration;
			}
			
			//Stop updating if speed is 0
			if(speed < 0)
				speed = 0;
			if(_isFirstTick){
				_isFirstTick = false;
			}else if(speed == 0){
				return;
			}

			//Road
			if(_currentLap <= _circuit.laps){//Current Lap
				if(_segmentProgress < _circuit.segmentArray.length-1){//Current Segment
					if(_currentSegmentY < 0){//Current Segment Index 
						_currentSegmentY = roadLines;
						
						_segmentProgress++;
						_currentSegment = Segment(_circuit.segmentArray[_segmentProgress - 1]).segmentType;
						_nextSegment = Segment(_circuit.segmentArray[_segmentProgress]).segmentType;
						
						dispatchEvent(new GameEvent(GameEvent.UPDATE_PROGRESS));
					}
						_linePosX = resX / 2;
						_linePosY = resY;
						
						_segmentCurveAmount = 0;
						_segmentSteepnessAmount = 0;
						
						//Update lines
						for (var i:int = 0; i < roadLines; i++)
						{
							//Set segment frame				
							var targetFrame:int = ((zMap[i] + _textureOffset) % 100 > 50) ? 1 : 2;
							lines[i].gotoAndStop(targetFrame);
							
							//Set segment position
							lines[i].x = _linePosX;
							lines[i].y = _linePosY;
							
							//Right
							if (_nextSegment == SegmentType.STRAIGHT && _currentSegment == SegmentType.CURVED_RIGHT)
							{
								_segmentCurveAmount +=  (i <= _currentSegmentY) ? _curveSharpness : -(_curveSharpness / 64);
							}
							else if (_currentSegment == SegmentType.STRAIGHT && _nextSegment == SegmentType.CURVED_RIGHT)
							{
								_segmentCurveAmount +=  (i >= _currentSegmentY) ? _curveSharpness : -(_curveSharpness / 64);
							}
	
							
							//Left
							if (_nextSegment == SegmentType.STRAIGHT && _currentSegment == SegmentType.CURVED_LEFT)
							{
								_segmentCurveAmount -=  (i <= _currentSegmentY) ? _curveSharpness * _controls.steeringValue : -(_curveSharpness / 64) * _controls.steeringValue ;
							}
							else if (_currentSegment == SegmentType.STRAIGHT && _nextSegment == SegmentType.CURVED_LEFT)
							{
								_segmentCurveAmount -=  (i >= _currentSegmentY) ? _curveSharpness * _controls.steeringValue  : -(_curveSharpness / 64) * _controls.steeringValue ;
							}
							
							_linePosX +=  _segmentCurveAmount;
							_linePosY +=  _segmentSteepnessAmount - 1;
						}
						
						//Update items						
						/*_startBanner.scaleX = _startBanner.scaleY = lines[110].scaleY;
						_startBanner.y += speed;*/
						
						_textureOffset = _textureOffset + speed;
						
						while (_textureOffset >= 100)
						{
							_textureOffset -=  100;
						}
						
						_currentSegmentY --;//Distance ?
						
						/*switch(_nextSegment){
							case SegmentType.STRAIGHT:
							case SegmentType.CURVED_LEFT:
							case SegmentType.CURVED_RIGHT:
								_steepness = 0;
								break;
							case SegmentType.UPHILL:
							case SegmentType.UPHILL_LEFT:
							case SegmentType.UPHILL_RIGHT:
								_steepness = 0.01;
								break;
							case SegmentType.STRAIGHT:
							case SegmentType.CURVED_LEFT:
							case SegmentType.CURVED_RIGHT:
								_steepness = -0.01;
								break;
						}*/
						
						trace("\ncurrentSegment : " + SegmentType.toString(_currentSegment) + " nextSegment : "+SegmentType.toString(_nextSegment));
				}else{
					dispatchEvent(new GameEvent(GameEvent.UPDATE_PROGRESS));
					trace("finished lap : "+_currentLap);
					_segmentProgress = 0;
					_currentLap ++;
				}
			}else{
				dispatchEvent(new GameEvent(GameEvent.UPDATE_PROGRESS));
				trace("finished race !");
			}			
		}
		
		public function get circuit():Sprite
		{
			return _circuitView;
		}
		
		public function get circuitWidth():Number
		{
			return resX;
		}
		
		public function get circuitHeight():Number
		{
			return resY;
		}

		public function get controls():Controls
		{
			return _controls;
		}

		public function set controls(value:Controls):void
		{
			_controls = value;
		}

	}
}