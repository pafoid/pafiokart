package com.pafoid.pafiokart.game.controllers
{
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.components.Controls;
	import com.pafoid.pafiokart.game.model.PlayerModel;
	import com.pafoid.pafiokart.interfaces.ITickable;
	
	import flash.display.MovieClip;

	public class CharacterController implements ITickable
	{
		private var _appFacade:AppFacade;
		private var _controls:Controls;
		private var _playerModel:PlayerModel;
		private var _character:MovieClip;
		
		private var _xBound:Number;
		
		public function CharacterController(appFacade:AppFacade, controls:Controls, playerModel:PlayerModel, character:MovieClip)
		{
			_appFacade = appFacade;			
			_controls = controls;
			_playerModel = playerModel;			
			_character = character;
			
			_xBound = _appFacade.stage.fullScreenWidth - _character.width;
		}
		
		public function tick():void{

		}
		
		public function destroy():void{
			_character = null;
			_controls = null;
			_appFacade = null;
		}
		
		private function get speedX():Number{
			return (Math.abs(_controls.steeringValue) >= 0.05) ? _controls.steeringValue : 0;
		}
	}
}