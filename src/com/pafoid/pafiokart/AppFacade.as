package com.pafoid.pafiokart
{
	import com.pafoid.debug.DebugSettings;
	import com.pafoid.pafiokart.manager.AssetManager;
	import com.pafoid.pafiokart.view.ViewController;
	import com.pafoid.ui.BaseAppFacade;
	
	import flash.display.Stage;
	import flash.text.Font;

	public class AppFacade extends BaseAppFacade
	{
		//Fonts
		[Embed(source="fonts/8-BIT WONDER.TTF", fontName ="pafoid EightBitWonder", embedAsCFF="false", unicodeRange='U+0020-U+007A, U+00C0-U+00F6, U+2019, U+002F', mimeType="application/x-font-truetype")]
		public var EightBitWonder:Class;

		private var _viewController:ViewController;
		private var _assetManager:AssetManager;
		
		public function AppFacade(app:PafioKart)
		{
			super(app);
			_assetManager = new AssetManager(this);
			_viewController = new ViewController(this);
			
			initSettings();
			initFonts();
		}
		
		private function initSettings():void
		{
			_debugSettings = new DebugSettings(this);
			_debugSettings.verboseAssetLoading = true;
		}
		
		private function initFonts():void
		{
			Font.registerFont(EightBitWonder);
		}
		
		public function get assetManager():AssetManager
		{
			return _assetManager;
		}

		public function get viewController():ViewController
		{
			return _viewController;
		}
	}
}