package com.pafoid.pafiokart.view
{
	import com.greensock.easing.Back;
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.screen.GameScreen;
	import com.pafoid.pafiokart.screen.Screen;
	import com.pafoid.pafiokart.screen.ScreenConstants;
	import com.pafoid.pafiokart.screen.TitleScreen;
	import com.pafoid.pafiokart.screen.events.ScreenEvent;
	import com.pafoid.pafiokart.transition.TransitionConstants;
	import com.pafoid.pafiokart.transition.TransitionController;
	import com.pafoid.pafiokart.transition.TransitionEvent;
	import com.pafoid.pafiokart.view.popup.Popup;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class ViewController extends EventDispatcher
	{
		private var _appFacade:AppFacade;
		private var _currentScreen:Screen;
		private var _previousScreen:Screen;
		private var _transitionController:TransitionController;
		private var _popup:Popup;
		
		public function ViewController(appFacade:AppFacade)
		{
			_appFacade = appFacade;
			_transitionController = new TransitionController(_appFacade);
			_appFacade.assetManager.addEventListener(Event.COMPLETE, onAssetManagerInitialized, false, 0, true);
		}
		
		protected function onAssetManagerInitialized(event:Event):void
		{
			_appFacade.assetManager.removeEventListener(Event.COMPLETE, onAssetManagerInitialized);
			changeScreen(ScreenConstants.TITLE_SCREEN);			
		}
		
		//Private methods
		private function changeScreen(screenName:String):void{
			var transition:String;
			var transitionDuration:Number = 0;
			var ease:Function;
			
			var hadPreviousScreen:Boolean = (_previousScreen != null);
			_previousScreen = _currentScreen;
			
			switch(screenName){
				case ScreenConstants.TITLE_SCREEN:
					_currentScreen = new TitleScreen(_appFacade);
					transition = (hadPreviousScreen) ? TransitionConstants.SLIDE_FROM_RIGHT : TransitionConstants.SIMPLE_SWAP;
					transitionDuration = 0.6;
					break;
				case ScreenConstants.GAME_SCREEN:
					_currentScreen = new GameScreen(_appFacade);
					transition = TransitionConstants.SLIDE_FROM_LEFT;
					transitionDuration = 0.6;
					break;
			}
			
			addEventListeners();
			
			_transitionController.startTransition(transition, _previousScreen, _currentScreen, transitionDuration, ease);
		}
		
		private function addEventListeners():void{
			_currentScreen.addEventListener(ScreenEvent.CHANGE_SCREEN, screenEventHandler, false, 0, true);
			_currentScreen.addEventListener(ScreenEvent.SHOW_POPUP, screenEventHandler, false, 0, true);
			_currentScreen.addEventListener(ScreenEvent.HIDE_POPUP, screenEventHandler, false, 0, true);
			_transitionController.addEventListener(TransitionEvent.TRANSITION_COMPLETE, _currentScreen.onTransitionComplete, false, 0, true);
		}
		
		private function showPopup(popupName:String):void{
			hidePopup();
			
			switch(popupName){
				/*case ScreenConstants.SETTINGS_POPUP:
					_popup = new SettingsPopup(_appFacade);
					break;*/
			}
			
			_currentScreen.showShadow();
			
			_popup.x = _appFacade.stage.stageWidth/2 - _popup.width/2;
			_popup.y = _appFacade.stage.stageHeight/2 - _popup.height/2;
			_currentScreen.addChild(_popup);
			_popup.show();
		}
		
		private function hidePopup(popupName:String = ""):void{
			if(_popup){
				if(popupName == "" || popupName == _popup.name){
					_currentScreen.hideShadow();
					_popup.hide();
					_currentScreen.removeChild(_popup);
					_popup.destroy();
					_popup = null;
				}
			}				
		}
		
		//Handlers
		private function screenEventHandler(event:ScreenEvent):void{
			switch(event.type){
				case ScreenEvent.CHANGE_SCREEN:
					changeScreen(event.screenName);
					break;
				case ScreenEvent.SHOW_POPUP:
					showPopup(event.screenName);
					break;
				case ScreenEvent.HIDE_POPUP:
					hidePopup(event.screenName);
					break;
			}
		}

		public function get currentScreen():Screen
		{
			return _currentScreen;
		}

	}
}