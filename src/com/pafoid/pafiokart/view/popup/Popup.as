package com.pafoid.pafiokart.view.popup
{
	import com.greensock.TweenLite;
	import com.pafoid.pafiokart.screen.Screen;
	import com.pafoid.pafiokart.screen.events.ScreenEvent;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import com.pafoid.pafiokart.AppFacade;
	
	public class Popup extends Screen
	{
		protected var _okButton:Sprite;
		
		protected var _popupWidth:Number;
		protected var _popupHeight:Number;
		
		public function Popup(appFacade:AppFacade, popupWidth:Number, popupHeight:Number)
		{
			super(appFacade, "popup", false);
			
			_popupWidth = popupWidth;
			_popupHeight = popupHeight;
			
			init();
		}
		
		protected function init():void{
			graphics.lineStyle(1, 0x000000, 1);
			graphics.beginFill(0xFFFFFF, 1);
			graphics.drawRect(0, 0, _popupWidth, _popupHeight);
			graphics.endFill();
		}
		
		//Public methods
		public function show():void{
			visible = true;
		}
		
		public function hide():void{
			visible = false;
		}
		
		override protected function onClickButton(event:MouseEvent):void{
			switch(event.currentTarget){
				case _okButton:
					dispatchEvent(new ScreenEvent(ScreenEvent.HIDE_POPUP, name));
					break;
			}	
		}		
		
		override public function destroy():void{
			super.destroy();
		}
	}
}