package com.pafoid.pafiokart.screen
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.pafoid.display.Color;
	import com.pafoid.display.TextFieldFactory;
	import com.pafoid.geometry.SquareCreator;
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.game.controllers.GameController;
	import com.pafoid.pafiokart.game.events.GameEvent;
	import com.pafoid.pafiokart.interfaces.ITickable;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Hud extends Sprite implements ITickable
	{
		private var _appFacade:AppFacade;
		private var _msgSprite:Sprite;
		private var _speedIndicator:TextField;
		private var _gameController:GameController;
		private var _uiFilter:DropShadowFilter = new DropShadowFilter(3, 45, Color.BLACK, 1, 0, 0);
		
		public function Hud(appFacade:AppFacade, gameController:GameController)
		{
			_appFacade = appFacade;
			_gameController = gameController;
			
			init();
		}
		
		private function init():void{
			//Speed Indicator
			var tf:TextFormat = new TextFormat("pafoid EightBitWonder", 24, Color.WHITE);
			_speedIndicator = TextFieldFactory.createTextField("", 10, 10, null, false, "left", this);
			_speedIndicator.width = 1000;
			_speedIndicator.embedFonts = true;
			_speedIndicator.defaultTextFormat = tf;
			_speedIndicator.text = "0 kph";
			_speedIndicator.filters = [_uiFilter];
			
			tick();
		}
		
		public function tick():void{
			_speedIndicator.text = Math.round(_gameController.speed).toString() + " kph";
		}
		
		public function showPreRaceMsg():void
		{
			_msgSprite = new Sprite();
			var tf:TextFormat = new TextFormat("pafoid EightBitWonder", 100, Color.WHITE);
			var textField:TextField = TextFieldFactory.createTextField("3", 0, 0, tf, false, "center", _msgSprite);
			textField.embedFonts = true;
			textField.defaultTextFormat = tf;
			textField.x = -textField.width/2;
			textField.y = -textField.height/2;
			textField.filters = [_uiFilter];
			_msgSprite.addChild(textField);
			addChild(_msgSprite);
			
			_msgSprite.x = (_appFacade.stage.fullScreenWidth) / 2;
			_msgSprite.y = (_appFacade.stage.fullScreenHeight) / 2 - _msgSprite.height;
			
			_msgSprite.scaleX = _msgSprite.scaleY = 0;
			
			var tween:TimelineLite = new TimelineLite();
			tween.append(new TweenLite(_msgSprite, 0.5, {scaleX:1, scaleY:1, delay:0.25}));
			tween.append(new TweenLite(_msgSprite, 0.5, {scaleX:0, scaleY:0, onComplete:function():void{textField.text ="2"}}));
			tween.append(new TweenLite(_msgSprite, 0.5, {scaleX:1, scaleY:1}));
			tween.append(new TweenLite(_msgSprite, 0.5, {scaleX:0, scaleY:0, onComplete:function():void{textField.text ="1"}}));
			tween.append(new TweenLite(_msgSprite, 0.5, {scaleX:1, scaleY:1}));
			tween.append(new TweenLite(_msgSprite, 0.5, {scaleX:0, scaleY:0, onComplete:function():void{textField.text ="GO !"}}));
			tween.append(new TweenLite(_msgSprite, 0.5, {scaleX:1, scaleY:1}));
			tween.append(new TweenLite(_msgSprite, 1, {scaleX:0, scaleY:0, onComplete:onPreRaceMsgComplete }));
			
			tween.play();			
		}
		
		private function onPreRaceMsgComplete():void{
			removeChild(_msgSprite);
			_msgSprite = null;
			dispatchEvent(new GameEvent(GameEvent.PRE_RACE_MSG_COMPLETE));
		}
		
		public function destroy():void{
			_appFacade = null;
		}
	}
}