package com.pafoid.pafiokart.screen
{
	import com.greensock.TweenLite;
	import com.pafoid.display.Color;
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.interfaces.ITickable;
	import com.pafoid.pafiokart.screen.events.ScreenEvent;
	import com.pafoid.pafiokart.transition.TransitionEvent;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class Screen extends Sprite
	{
		protected var _id:String;
		protected var _appFacade:AppFacade;
		protected var _buttons:Array;
		protected var _textFields:Array;
		protected var _titleTF:TextField;
		protected var _shadow:Sprite;
		protected var _registeredForTick:Array = [];
		
		public function Screen(appFacade:AppFacade, id:String, isFullScreen:Boolean = true)
		{
			_appFacade = appFacade;
			_id = id;

			if(isFullScreen){
				graphics.beginFill(0x000000, 0);
				graphics.drawRect(0, 0, _appFacade.stage.fullScreenWidth, _appFacade.stage.fullScreenHeight);
				graphics.endFill();
			}
			
			addEventListener(Event.COMPLETE, onAssetsLoaded, false, 0, true);
			loadAssets();
		}
		
		protected function initUI():void{
			throw new Error("This function needs to be overriden.");
		}
		
		public function onTransitionComplete(event:TransitionEvent):void{
		}
		
		protected function loadAssets():void{
			_appFacade.assetManager.loadAssets(_id, initUI);
		}
		
		private function onAssetsLoaded(event:Event):void{
			initUI();
		}
		
		protected function startTicking():void{
			_appFacade.stage.addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
		}
		
		protected function stopTicking():void{
			_appFacade.stage.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		protected function onEnterFrame(event:Event):void
		{
			for each (var tickable:ITickable in _registeredForTick) 
			{
				tickable.tick();
			}			
		}	
		
		public function registerForTicks(tickable:ITickable):void{
			_registeredForTick.push(tickable);
		}
		
		protected function onClickButton(event:MouseEvent):void{
			throw new Error("This function needs to be overriden.");	
		}
		
		public function showShadow():void{
			_shadow ||= new Sprite();
			_shadow.graphics.beginFill(0x000000, 0.6);
			_shadow.graphics.drawRect(0, 0, _appFacade.stage.stageWidth, _appFacade.stage.stageHeight);
			_shadow.graphics.endFill();
			
			_shadow.alpha = 0;
			_appFacade.viewController.currentScreen.addChild(_shadow);
			TweenLite.to(_shadow, 0.3, {alpha:1});
		}
		
		public function hideShadow():void{
			if(_shadow)
				TweenLite.to(_shadow, 0.3, {alpha:0, onComplete:destroyShadow});
		}
		
		protected function destroyShadow():void{
			if(_shadow){
				_appFacade.viewController.currentScreen.removeChild(_shadow);
				_shadow = null;
			}
		}
		
		public function center(item:DisplayObject):void{
			centerHorizontaly(item);
			centerVerticaly(item);
		}
		
		public function centerHorizontaly(item:DisplayObject):Number{
			item.x = (_appFacade.stage.fullScreenWidth - item.width) / 2;
			return item.x;
		}
		
		public function centerVerticaly(item:DisplayObject):Number{
			item.y = (_appFacade.stage.fullScreenHeight - item.height) / 2;
			return item.y;
		}
		
		protected function onOverButton(event:MouseEvent):void{
			var button:Sprite = event.currentTarget as Sprite;
			var tf:TextField = button.getChildAt(0) as TextField;
			tf.textColor = Color.ORANGE;
		}
		
		protected function onOutButton(event:MouseEvent):void{
			var button:Sprite = event.currentTarget as Sprite;
			var tf:TextField = button.getChildAt(0) as TextField;
			tf.textColor = Color.WHITE;
		}
		
		public function destroy():void{
			stopTicking();			
			_appFacade = null;
			
			if(_buttons){
				for each (var button:Sprite in _buttons) 
				{
					if(button){
						removeChild(button);
						button.removeEventListener(MouseEvent.CLICK, onClickButton);
						button = null;
					}	
				}
				
				_buttons = null;
			}
			
			if(_textFields){
				for each (var textField:TextField in _textFields) 
				{
					if(textField){
						removeChild(textField);
						textField = null;
					}
				}
			}
		}
	}
}