package com.pafoid.pafiokart.screen
{
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.components.Controls;
	import com.pafoid.pafiokart.components.MiniMap;
	import com.pafoid.pafiokart.game.controllers.CharacterController;
	import com.pafoid.pafiokart.game.controllers.GameController;
	import com.pafoid.pafiokart.game.events.GameEvent;
	import com.pafoid.pafiokart.game.managers.CircuitManager;
	import com.pafoid.pafiokart.game.model.PlayerModel;
	import com.pafoid.pafiokart.game.model.RaceModel;
	import com.pafoid.pafiokart.transition.TransitionEvent;
	
	import flash.display.MovieClip;
	
	public class GameScreen extends Screen
	{
		//Models
		private var _raceModel:RaceModel;
		private var _playerModel:PlayerModel;
		
		//Managers/Controllers
		private var _gameController:GameController;
		private var _circuitManager:CircuitManager;
		
		//Game
		private var _character:MovieClip;
		private var _characterController:CharacterController;
		
		//UI
		private var _background:MovieClip;
		private var _miniMap:MiniMap;
		private var _controls:Controls;
		private var _hud:Hud;
		
		public function GameScreen(appFacade:AppFacade)
		{
			super(appFacade, ScreenConstants.GAME_SCREEN);
			
			_raceModel = new RaceModel();
			_playerModel = new PlayerModel();
			_circuitManager = new CircuitManager(_appFacade, _raceModel); 
			_controls = new Controls(_appFacade, this);
		}
		
		override protected function initUI():void
		{
			//Background
			_background = _appFacade.assetManager.getAssetById("background");
			_background.width = _appFacade.stage.fullScreenWidth;
			_background.height = _appFacade.stage.fullScreenHeight;
			addChild(_background);

			//Circuit
			_gameController = new GameController(_appFacade, _raceModel.getCircuitById(0), _playerModel, _controls);
			registerForTicks(_gameController);
			addChild(_gameController.circuit);
			
			_gameController.circuit.scaleX = _appFacade.stage.fullScreenWidth / _gameController.circuitWidth;
			_gameController.circuit.scaleY = _appFacade.stage.fullScreenHeight / _gameController.circuitHeight;

			//MiniMap
		/*	_miniMap = new MiniMap(_appFacade, circuit, _circuitController);
			_miniMap.x = _miniMap.width;
			_miniMap.y = _miniMap.height;
			_circuitController.addEventListener(CircuitEvent.UPDATE_PROGRESS, _miniMap.updateMap, false, 0, true);
			addChild(_miniMap);*/
			
			//Character
			_character = _appFacade.assetManager.getAssetById("Mario");
			_character.scaleX = _character.scaleY = Math.min(_gameController.circuit.scaleX, _gameController.circuit.scaleY);
			addChild(_character);
			centerHorizontaly(_character);
			_character.x += _character.width /2;
			_character.y = _appFacade.stage.fullScreenHeight * 5 / 6;
			_characterController = new CharacterController(_appFacade, _controls, _playerModel, _character);
			registerForTicks(_characterController);
			
			//Controls
			_controls.addToStage();
			
			//HUD
			_hud = new Hud(_appFacade, _gameController);
			registerForTicks(_hud);
			addChild(_hud);
		}		
		
		override public function onTransitionComplete(event:TransitionEvent):void{
			_hud.addEventListener(GameEvent.PRE_RACE_MSG_COMPLETE, startRace, false, 0, true);
			_hud.showPreRaceMsg();
		}
		
		private function startRace(event:GameEvent):void{
			_hud.removeEventListener(GameEvent.PRE_RACE_MSG_COMPLETE, startRace);
			startTicking();	
		}
		
		override public function destroy():void{
			_controls.destroy();
			_controls = null;
			
			super.destroy();
		}
		
	}
}