package com.pafoid.pafiokart.screen
{
	import com.pafoid.display.ButtonFactory;
	import com.pafoid.display.TextFieldFactory;
	import com.pafoid.pafiokart.AppFacade;
	import com.pafoid.pafiokart.screen.events.ScreenEvent;
	import com.pafoid.ui.controls.dpad.DpadControl;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	public class TitleScreen extends Screen
	{
		private var _startBtn:Sprite;
		private var _settingsBtn:Sprite; 
		
		public function TitleScreen(appFacade:AppFacade)
		{
			super(appFacade, ScreenConstants.TITLE_SCREEN);
		}
		
		override protected function initUI():void
		{
			//Title
			var tf:TextFormat = new TextFormat("pafoid EightBitWonder", 50, 0x000000, true);
			_titleTF = TextFieldFactory.createTextField("Pafio Kart", 0, 150, tf, false, TextFieldAutoSize.LEFT, this);
			
			centerHorizontaly(_titleTF);
			_textFields = [_titleTF];
			
			//Buttons
			tf.size = 30;
			_startBtn = ButtonFactory.createButton(this, "Start", 200, 60, 0, 0, 30, true, onClickButton, onOverButton, onOutButton, tf);
			center(_startBtn);
			tf.size = 12;
			_settingsBtn = ButtonFactory.createButton(this, "Settings", 100, 30, 50, 25, -1, true, onClickButton, onOverButton, onOutButton, tf);
			
			_buttons = [_startBtn, _settingsBtn];
		}
		
		override protected function onClickButton(event:MouseEvent):void{
			switch(event.currentTarget){
				case _startBtn:
					dispatchEvent(new ScreenEvent(ScreenEvent.CHANGE_SCREEN, ScreenConstants.GAME_SCREEN));
					break;
				case _settingsBtn:
					dispatchEvent(new ScreenEvent(ScreenEvent.SHOW_POPUP, ScreenConstants.SETTINGS_POPUP));
					break;
			}
		}
		
		override public function destroy():void{
			super.destroy();
		}
	}
}