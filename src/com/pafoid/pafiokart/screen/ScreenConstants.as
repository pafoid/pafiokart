package com.pafoid.pafiokart.screen
{
	public class ScreenConstants
	{
		public static const TITLE_SCREEN:String = "titleScreen";
		public static const GAME_SCREEN:String = "gameScreen";
		
		public static const SETTINGS_POPUP:String = "settingsPopup";
	}
}